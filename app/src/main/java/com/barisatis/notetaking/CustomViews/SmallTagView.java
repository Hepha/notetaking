package com.barisatis.notetaking.CustomViews;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.barisatis.notetaking.Classes.Tag;
import com.barisatis.notetaking.R;

/**
 * Created by Hepha on 16.11.2016.
 */
public class SmallTagView extends LinearLayout {

    Tag tag;
    Boolean isRemove;

    TextView tvIcon, tvName;

    public SmallTagView(Context context, Tag tag, boolean isRemove) {
        super(context);

        this.tag = tag;
        this.isRemove = isRemove;

        initializeViews(context);
    }

    public Tag getTag(){
        return this.tag;
    }

    private void initializeViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.small_tag_view, this);

        //Call the onFinishInflate because it doesn't get called by itself for some reason TODO:Find out why
        onFinishInflate();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        tvIcon = (TextView)findViewById(R.id.tvIcon);
        tvName = (TextView)findViewById(R.id.tvName);

        tvName.setText(tag.getName());

        if(isRemove)
            tvIcon.setText("X");

    }

    public Boolean IsRemove() {
        return isRemove;
    }
}
