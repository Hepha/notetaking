package com.barisatis.notetaking.Classes;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class Note {

    private int id;
    private String title, entry;
    private Date createdAt, updatedAt;
    private List<Tag> tagList;

    //region constructors

    public Note(int id, String title, String entry, Date createdAt, Date updatedAt) {
        this.id = id;
        this.title = title;
        this.entry = entry;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        tagList = new ArrayList<>();
    }

    public Note(String title, String entry, Date createdAt) {
        id = -1;
        this.title = title;
        this.entry = entry;
        this.createdAt = createdAt;
        this.updatedAt = createdAt;
        tagList = new ArrayList<>();
    }
    //endregion

    //region Getters setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEntry() {
        return entry;
    }

    public void setEntry(String entry) {
        this.entry = entry;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }

    //endregion


    public void UpdateAtDB(){
    }
}
