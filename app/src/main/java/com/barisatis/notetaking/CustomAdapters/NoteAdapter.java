package com.barisatis.notetaking.CustomAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.barisatis.notetaking.Classes.Note;
import com.barisatis.notetaking.R;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Hepha on 19.11.2016.
 */
public class NoteAdapter extends ArrayAdapter<Note> {

    public NoteAdapter(Context context, int resource) {
        super(context, resource);
    }

    public NoteAdapter(Context context, int resource, List<Note> objects) {
        super(context, resource, objects);
    }

    public NoteAdapter(Context context, int resource, Note[] objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if(v==null){
            LayoutInflater li = LayoutInflater.from(getContext());
            v = li.inflate(R.layout.adapter_note, null);
        }

        Note note = getItem(position);

        TextView tvTitle = (TextView)v.findViewById(R.id.tvTitle);
        TextView tvDate = (TextView)v.findViewById(R.id.tvDate);
        TextView tvEntry = (TextView)v.findViewById(R.id.tvEntry);
        TextView tvTags = (TextView)v.findViewById(R.id.tvTags);

        if(note!=null){
            //if title string is empty make tvtitle visibility gone
            if(tvTitle!=null){
                if(!note.getTitle().isEmpty())
                    tvTitle.setText(note.getTitle());
                else
                    tvTitle.setVisibility(View.GONE);
            }
            if(tvDate!=null){
                DateFormat dateFormat = DateFormat.getDateInstance();
                tvDate.setText("Created At:" + dateFormat.format(note.getCreatedAt()));
            }
            if(tvEntry!=null){
                tvEntry.setText(note.getEntry());
            }
            if(tvTags!=null){
                String s = "Tags: ";
                for(int i = 0; i<note.getTagList().size();i++){
                    s += note.getTagList().get(i).getName();
                    if(i!=(note.getTagList().size()-1))
                        s += ", ";
                    else
                        break;
                }
                tvTags.setText(s);
            }
        }

        return v;
    }
}
