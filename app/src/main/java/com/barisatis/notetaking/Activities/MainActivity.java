package com.barisatis.notetaking.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.barisatis.notetaking.Classes.*;
import com.barisatis.notetaking.CustomAdapters.NoteAdapter;
import com.barisatis.notetaking.CustomDialogs.AddTagDialog;
import com.barisatis.notetaking.CustomViews.SmallTagView;
import com.barisatis.notetaking.Helpers.*;
import com.barisatis.notetaking.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    //region View variables
    //Views
    private LinearLayout llMain, barContent1, barContent2, llNewNote;
    private RelativeLayout barContainer;

    private LinearLayout llAddedTags;

    private ImageButton ibNewCancel, ibNewDone;

    private EditText etNewTitle, etNewEntry, etBar1Dummy;

    private Button btnNewAddTag;

    private TextView tvTagWarning;

    private ListView lvMain;

    //endregion

    //DB
    DatabaseHandler db;

    //Tag lists
    private List<Tag> tagList, tagAddedList, tagRemainigList;

    //Note list
    private List<Note> noteList;

    //Adapters
    private NoteAdapter noteAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    //region initializers

    //Calls each of the init methods
    private void init(){

        initViews();
        initDB();
        initEventHandlers();
        initAdapters();

    }

    //initializer for loading view variables
    private void initViews(){

        //layouts
        llMain = (LinearLayout)findViewById(R.id.llMain);
        llNewNote = (LinearLayout)findViewById(R.id.llNewNote);
        barContent1 = (LinearLayout)findViewById(R.id.barContent1);
        barContent2 = (LinearLayout)findViewById(R.id.barContent2);
        barContainer = (RelativeLayout) findViewById(R.id.barContainer);

        llAddedTags = (LinearLayout)findViewById(R.id.llAddedTags);


        //
        ibNewCancel = (ImageButton)findViewById(R.id.ibNewCancel);
        ibNewDone = (ImageButton)findViewById(R.id.ibNewDone);

        etNewTitle = (EditText) findViewById(R.id.etNewTitle);
        etNewEntry = (EditText) findViewById(R.id.etNewEntry);
        etBar1Dummy = (EditText) findViewById(R.id.etBar1Dummy);

        tvTagWarning = (TextView)findViewById(R.id.tvTagWarning);

        btnNewAddTag = (Button)findViewById(R.id.btnNewAddTag);

        lvMain = (ListView)findViewById(R.id.lvMain);

    }

    private void fillTagViews(){
        //Clear the view
        llAddedTags.removeAllViews();

        //Add the views
        for(int i = 0; i<tagAddedList.size();i++){
            SmallTagView stv = new SmallTagView(this, tagAddedList.get(i), true);
            llAddedTags.addView(stv);
        }
    }

    private void initDB(){

        db = new DatabaseHandler(this);

        tagList = db.getAllTags();

        noteList = db.getAllNotes();

    }

    private void initEventHandlers(){

        //onClick
        barContent1.setOnClickListener(this);

        ibNewCancel.setOnClickListener(this);
        ibNewDone.setOnClickListener(this);

        etBar1Dummy.setOnClickListener(this);

        btnNewAddTag.setOnClickListener(this);

    }

    private void initAdapters(){
        //Note adapter
        noteAdapter = new NoteAdapter(this, R.layout.adapter_note, noteList);
        lvMain.setAdapter(noteAdapter);
    }

    //endregion

    //close the soft keyboard
    private void closeKeyboard(){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void addNewNote(){
        //Get the values
        String title = etNewTitle.getText().toString().trim();
        String entry = etNewEntry.getText().toString();
        if(entry.isEmpty()){
            giveError("Entry cannot be empty!");
            return;
        }
        //Create the note object
        Note note = new Note(title, entry, NtUtil.getCurrentDate());
        //save to db
        db.addNote(note);
        //Add to list
        noteList.add(0, note);

        //Close the new note view
        closeNewNoteView();

        //Redraw main list
        noteAdapter.notifyDataSetChanged();
    }

    private void openNewNoteView(){
        //switch bar contents
        barContent1.setVisibility(View.GONE);
        barContent2.setVisibility(View.VISIBLE);

        //Open new note layout
        llNewNote.setVisibility(View.VISIBLE);

        //Init the tag lists
        tagAddedList = new ArrayList<>();
        tagRemainigList = new ArrayList<>();
        tagRemainigList.addAll(tagList);

        //TODO: animate the switch

        //TODO: open the keyboard
    }

    private void closeNewNoteView(){
        //switch bar contents
        barContent1.setVisibility(View.VISIBLE);
        barContent2.setVisibility(View.GONE);

        //close new note layout
        llNewNote.setVisibility(View.GONE);

        // close the keyboard
        closeKeyboard();

        //TODO: animate the switch

        //clear the views
        etNewEntry.setText("");
        etNewTitle.setText("");
        llAddedTags.removeAllViews();

        //Clear the lists
        tagAddedList.clear();
        tagRemainigList.clear();
        tagRemainigList.addAll(tagList);
    }

    //TODO: create a custom way of giving errors instead of just toasts
    private void giveError(String s){
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View view) {

        if(view == barContent1 || view == etBar1Dummy){

            openNewNoteView();

        }
        else if(view == ibNewCancel){

            closeNewNoteView();

        }
        else if(view == ibNewDone){
            //TODO: check the new note for errors

            addNewNote();
        }
        else if(view == btnNewAddTag){

            //init dialog
            final AddTagDialog dialog = new AddTagDialog(this, tagAddedList, tagRemainigList, db, new AddTagDialog.dialogInterface() {
                @Override
                public void newAdded(Tag tag) {
                    tagList.add(tag);
                }
            });

            //Set the action that will happen when the view is dismissed
            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    //Set the lists
                    tagAddedList = dialog.getAddedList();
                    tagRemainigList = dialog.getRemainingList();

                    //Set the added list view
                    fillTagViews();
                }
            });

            dialog.show();

        }

    }
}
