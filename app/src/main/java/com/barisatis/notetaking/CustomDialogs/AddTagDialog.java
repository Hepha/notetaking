package com.barisatis.notetaking.CustomDialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

import com.barisatis.notetaking.Classes.Tag;
import com.barisatis.notetaking.CustomViews.SmallTagView;
import com.barisatis.notetaking.Helpers.DatabaseHandler;
import com.barisatis.notetaking.R;

import java.util.List;

/**
 * Created by Hepha on 16.11.2016.
 */
public class AddTagDialog extends Dialog implements OnClickListener{

    private List<Tag> addedList, remainingList;

    private HorizontalScrollView svAdded, svRemaining;

    private LinearLayout llAdded,  llRemaining;

    private Button btnNewtag;

    private DatabaseHandler db;

    private dialogInterface mCallbacks;


    public AddTagDialog(Context context, List<Tag> addedList, List<Tag> remainingList, DatabaseHandler db, dialogInterface callbacks) {
        super(context);
        this.addedList = addedList;
        this.remainingList = remainingList;
        this.db = db;

        this.mCallbacks = callbacks;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.add_tag_dialog);

        init();
    }

    private void init(){
        svAdded = (HorizontalScrollView)findViewById(R.id.svAdded);
        svRemaining = (HorizontalScrollView)findViewById(R.id.svRemaining);

        llAdded = (LinearLayout)findViewById(R.id.llAdded);
        llRemaining = (LinearLayout)findViewById(R.id.llRemaining);

        btnNewtag = (Button)findViewById(R.id.btnNewTag);
        btnNewtag.setOnClickListener(this);


        fillScrollViews();
    }

    private void fillScrollViews(){

        //Empty the layouts
        llAdded.removeAllViews();
        llRemaining.removeAllViews();

        //Remaining view
        for(int i = 0; i<remainingList.size();i++){
            Tag tag = remainingList.get(i);

            addToRemainingSV(tag);
        }

        //Added view
        for(int i = 0; i<addedList.size();i++){
            Tag tag = addedList.get(i);

            addToAddedSV(tag);
        }

    }

    //region Add-Romve SMalTagViews from the SCrollviews
    private void addToAddedSV(Tag tag){

        SmallTagView stv = new SmallTagView(getContext(), tag, true);
        stv.setOnClickListener(this);

        llAdded.addView(stv);

    }

    private void removeFromAddedSV(int index){
        llAdded.removeViewAt(index/2);

    }

    private void addToRemainingSV(Tag tag){

        SmallTagView stv = new SmallTagView(getContext(), tag, false);
        stv.setOnClickListener(this);

        llRemaining.addView(stv);


    }

    private void removeFromRemainingSV(int index){

        llRemaining.removeViewAt(index/2);


    }
    //endregion

    public List<Tag> getAddedList() {
        return addedList;
    }

    public List<Tag> getRemainingList() {
        return remainingList;
    }

    private void switchTag(int index, boolean toAdded){

        if(toAdded){

            //Add to added list
            addedList.add(remainingList.get(index));
            //Remove from the remaining list
            remainingList.remove(index);
            //Remove from the remaining view
            //removeFromRemainingSV(index);
            //Add to added view
            //addToAddedSV(tag);

        }else{

            //Add to remaining
            remainingList.add(addedList.get(index));
            //Remove from the added list
            addedList.remove(index);
            //Remove from the added view
            //removeFromAddedSV(index);
            //Add to remaining view
            //addToRemainingSV(tag);

        }

        //Call the fill method to redraw
        fillScrollViews();
    }

    private void CreateNewTagDialog(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());

        final EditText et = new EditText(getContext());
        dialog.setMessage("Enter the tag");
        dialog.setTitle("New Tag");

        dialog.setView(et);

        dialog.setPositiveButton("Done", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String value = et.getText().toString().trim();

                //Check if empty
                if(!value.isEmpty()){
                    //Check if it exists
                    if(!db.tagExistsWithName(value)){
                        //TODO:Remove the empty warning

                        Tag tag = new Tag(value);

                        db.addTag(tag);
                        addedList.add(tag);
                        mCallbacks.newAdded(tag);
                        addToAddedSV(tag);
                    }
                    else{
                        //TODO: toast error or something
                    }

                }else{
                    //TODO:toast error maybe
                }
            }
        });

        dialog.setNegativeButton("Cancel", null);

        dialog.show();
    }

    @Override
    public void onClick(View view) {

        if(view instanceof SmallTagView){
            SmallTagView stv = (SmallTagView)view;

            Log.d("Clicked", stv.getTag().getName());

            //Check if it comes from added or remaining
            if(stv.IsRemove()){
                int index = llAdded.indexOfChild(stv);
                switchTag(index, false);
            }else{
                int index = llRemaining.indexOfChild(stv);
                switchTag(index, true);
            }
        }
        else if(view == btnNewtag){
            CreateNewTagDialog();
        }

    }

    //Interface for callbacks
    public interface dialogInterface{
        public void newAdded(Tag tag);
    }
}
