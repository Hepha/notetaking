package com.barisatis.notetaking.Helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.barisatis.notetaking.Classes.*;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;


public class DatabaseHandler extends SQLiteOpenHelper{


    //region VARIABLES

    //DATABASE VALUES
    private static final String DATABASE_NAME = "ntData";
    private static final int DATABASE_VERSION = 1;

    //NOTES TABLE VARIABLES
    private static final String TABLE_NOTE = "notes";

    private static final String COLUMN_NOTE_ID = "id";
    private static final String COLUMN_NOTE_TITLE = "title";
    private static final String COLUMN_NOTE_ENTRY = "entry";
    private static final String COLUMN_NOTE_CREATED_AT = "createdAt";
    private static final String COLUMN_NOTE_UPDATED_AT = "updatedAt";

    //TAG TABLE VARIABLES
    private static final String TABLE_TAG = "tags";

    private static final String COLUMN_TAG_ID = "id";
    private static final String COLUMN_TAG_NAME = "name";

    //NOTETAG TABLE VARIABLES
    private static final String TABLE_NOTE_TAG = "noteTag";

    private static final String COLUMN_NOTE_TAG_NOTE_ID = "noteId";
    private static final String COLUMN_NOTE_TAG_TAG_ID = "tagId";

    //endregion


    //Constructor
    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Create notes table
        String createNotes = "CREATE TABLE " + TABLE_NOTE + " (" + COLUMN_NOTE_ID + " INTEGER PRIMARY KEY, " + COLUMN_NOTE_TITLE + " TEXT, " + COLUMN_NOTE_ENTRY + " TEXT, "
                + COLUMN_NOTE_CREATED_AT + " INTEGER, " + COLUMN_NOTE_UPDATED_AT + " INTEGER " + ")";
        db.execSQL(createNotes);

        //Create tags table
        String createTags = "CREATE TABLE " + TABLE_TAG + " (" + COLUMN_TAG_ID + " INTEGER PRIMARY KEY, " + COLUMN_TAG_NAME + " TEXT " + ")";
        db.execSQL(createTags);

        //Create noteTag table
        String createNoteTag = "CREATE TABLE " + TABLE_NOTE_TAG + " (" + COLUMN_NOTE_TAG_NOTE_ID + " INTEGER, " + COLUMN_NOTE_TAG_TAG_ID + " INTEGER " + ")";
        db.execSQL(createNoteTag);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }



    //region NOTES TABLE

    //NOTE TABLE HANDLER FUNCTIONS
    public void addNote(Note note){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_NOTE_TITLE, note.getTitle());
        values.put(COLUMN_NOTE_ENTRY, note.getEntry());
        values.put(COLUMN_NOTE_CREATED_AT, note.getCreatedAt().getTime());
        values.put(COLUMN_NOTE_UPDATED_AT, note.getUpdatedAt().getTime());

        db.insert(TABLE_NOTE, null, values);

        db.close();
    }

    //TODO: Have both of the queries inside one query
    public Note getNote(int id){
        SQLiteDatabase db = this.getReadableDatabase();

        //Create a query with the id
        String query = "SELECT * FROM " + TABLE_NOTE + " WHERE " + COLUMN_NOTE_ID  + " = " + id;

        //Cursor cursor = db.query(TABLE_NOTE, new String[]{COLUMN_NOTE_ID, COLUMN_NOTE_TITLE, COLUMN_NOTE_ENTRY, COLUMN_NOTE_CREATED_AT, COLUMN_NOTE_UPDATED_AT}, );
        Cursor cursor = db.rawQuery(query, null);

        if(cursor!=null) cursor.moveToFirst();

        Note note = new Note(cursor.getInt(0),
                cursor.getString(1),
                cursor.getString(2),
                new Date(cursor.getLong(3)),
                new Date(cursor.getLong(4)));

        //close cursor
        if(cursor!=null && !cursor.isClosed())
            cursor.close();

        //Get the Tags
        String tagQuery = "SELECT * FROM " + TABLE_TAG + " INNER JOIN " + TABLE_NOTE_TAG
                + " ON " + TABLE_NOTE_TAG + "." + COLUMN_NOTE_TAG_TAG_ID + " = " + TABLE_TAG + "." + COLUMN_TAG_ID +
                " WHERE " + TABLE_NOTE_TAG + "." + COLUMN_NOTE_TAG_NOTE_ID + " = " + id;

        ArrayList<Tag> tagList = new ArrayList<>();

        Cursor tagCursor = db.rawQuery(query, null);

        if(tagCursor.moveToFirst()){
            do{
                Tag tag = new Tag(tagCursor.getInt(0), tagCursor.getString(1));
                tagList.add(tag);
            }while (tagCursor.moveToNext());
        }

        note.setTagList(tagList);

        //close tagCursor
        if(tagCursor!=null && !tagCursor.isClosed())
            tagCursor.close();

        db.close();

        return note;
    }

    //TODO: have custom orderings with a parameter for it
    public List<Note> getAllNotes(){
        SQLiteDatabase db = this.getReadableDatabase();


        String query = "SELECT * FROM " + TABLE_NOTE + " ORDER BY " + COLUMN_NOTE_CREATED_AT + " DESC";

        ArrayList<Note> list = new ArrayList<Note>();

        Cursor cursor =  db.rawQuery(query, null);

        if(cursor.moveToFirst()){
            do{
                Note note = new Note(cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        new Date(cursor.getLong(3)),
                        new Date(cursor.getLong(4)));
                list.add(note);
            }while(cursor.moveToNext());
        }

        //close everything
        if(cursor!=null && !cursor.isClosed())
            cursor.close();

        db.close();

        return list;
    }

    //Get function with tags
    public List<Note> getNotesWithTag(int tagId){
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT * FROM " + TABLE_NOTE + " INNER JOIN " + TABLE_NOTE_TAG
                + " ON " + TABLE_NOTE_TAG + "." + COLUMN_NOTE_TAG_NOTE_ID + " = " + TABLE_NOTE + "." + COLUMN_NOTE_ID +
                " WHERE " + TABLE_NOTE_TAG + "." + COLUMN_NOTE_TAG_TAG_ID + " = " + tagId;

        ArrayList<Note> list = new ArrayList<Note>();

        Cursor cursor =  db.rawQuery(query, null);

        if(cursor.moveToFirst()){
            do{
                Note note = new Note(cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        new Date(cursor.getLong(3)),
                        new Date(cursor.getLong(4)));
                list.add(note);
            }while(cursor.moveToNext());
        }

        //close everything
        if(cursor!=null && !cursor.isClosed())
            cursor.close();

        db.close();

        return list;
    }

    public void updateNote(Note note){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(COLUMN_NOTE_TITLE, note.getTitle());
        values.put(COLUMN_NOTE_ENTRY, note.getEntry());
        values.put(COLUMN_NOTE_CREATED_AT, note.getCreatedAt().getTime());
        values.put(COLUMN_NOTE_UPDATED_AT, note.getUpdatedAt().getTime());

        db.update(TABLE_NOTE, values, COLUMN_NOTE_ID + "=?", new String[]{String.valueOf(note.getId())});

        db.close();
    }

    public void deleteNote(Note note){
        //call the one with id parameter
        deleteNote(note.getId());
    }

    public void deleteNote(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NOTE, COLUMN_NOTE_ID + "=?", new String[]{String.valueOf(id)});
        db.close();
    }
    //endregion



    //region TAG TABLE

    //TAG TABLE HANDLER FUNCTIONS

    public void addTag(Tag tag){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(COLUMN_TAG_NAME, tag.getName());

        db.insert(TABLE_TAG,  null, values);

        db.close();
    }

    public Tag getTag(int id){
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT * FROM " + TABLE_TAG + " WHERE " + COLUMN_TAG_ID + " = " + id;

        Cursor cursor = db.rawQuery(query, null);

        if(cursor!=null) cursor.moveToFirst();
        else return null;

        Tag tag = new Tag(cursor.getInt(0), cursor.getString(1));

        //close everything
        if(cursor!=null && !cursor.isClosed())
            cursor.close();

        db.close();

        return tag;
    }

    public List<Tag> getAllTags(){
        SQLiteDatabase db = this.getReadableDatabase();

        String query = " SELECT * FROM " + TABLE_TAG;

        ArrayList<Tag> list = new ArrayList<>();

        Cursor cursor = db.rawQuery(query, null);

        if(cursor.moveToFirst()){
            do{
                Tag tag = new Tag(cursor.getInt(0), cursor.getString(1));
                list.add(tag);
            }while (cursor.moveToNext());
        }
        //close everything
        if(cursor!=null && !cursor.isClosed())
            cursor.close();

        db.close();


        return list;
    }

    public void updateTag(Tag tag){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(COLUMN_TAG_NAME, tag.getName());

        db.update(TABLE_TAG, values, COLUMN_TAG_ID + "=?", new String[]{String.valueOf(tag.getId())});

        db.close();
    }

    public void deleteTag(Tag tag){
        deleteTag(tag.getId());
    }

    public void deleteTag(int id){
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_TAG, COLUMN_TAG_ID + "=?", new String[]{String.valueOf(id)});

        db.close();
    }

    //To check if a tag with given name already exists
    public boolean tagExistsWithName(String name){
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT * FROM " + TABLE_TAG + " WHERE " + COLUMN_TAG_NAME + " = '" + name + "'";

        Cursor cursor = db.rawQuery(query, null);

        boolean b = false;

        if(cursor.moveToFirst())
            b = true;

        //close everything
        if(cursor!=null && !cursor.isClosed())
            cursor.close();

        db.close();

        return b;
    }

    //endregion



    //region NOTETAG TABLE

    public void addNoteTag(int noteId, int tagId){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(COLUMN_NOTE_TAG_NOTE_ID, noteId);
        values.put(COLUMN_NOTE_TAG_TAG_ID, tagId);

        db.insert(TABLE_NOTE_TAG, null, values);
    }

    public void deleteNoteTag(int noteId, int tagId){
        SQLiteDatabase db = this.getWritableDatabase();

        String query = "DELETE FROM " + TABLE_NOTE_TAG + " WHERE " + COLUMN_NOTE_TAG_NOTE_ID + " = " + noteId + " AND " + COLUMN_NOTE_TAG_TAG_ID + " = " + tagId;

        db.execSQL(query);

        db.close();
    }

    //endregion

}

